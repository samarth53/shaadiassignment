package com.shaadi.people.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shaadi.people.Helper.DatabaseHelper;
import com.shaadi.people.Models.Profile;
import com.shaadi.people.Models.Result;
import com.shaadi.people.R;
import com.shaadi.people.Utils.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private Context context;
    private List<Result> results;
    DatabaseHelper databaseHelper;

    public HomeAdapter(Context context, List<Result> results){
        this.context = context;
        this.results = results;
        databaseHelper = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Picasso.with(context).load(results.get(position).getPicture().getLarge()).into(holder.mImgProfile);
        holder.mName.setText(results.get(position).getName().getTitle()+" "+results.get(position).getName().getFirst()+" "+results.get(position).getName().getLast());
        holder.mAddress.setText(results.get(position).getLocation().getStreet().getNumber()+", "+results.get(position).getLocation().getStreet().getName()+", "+results.get(position).getLocation().getCity()+", "+results.get(position).getLocation().getState()+", "+results.get(position).getLocation().getCountry());
        holder.mGender.setText(results.get(position).getGender());
        holder.mEmail.setText(results.get(position).getEmail());
        holder.mDob.setText(Utility.date(results.get(position).getDob().getDate()));
        holder.mAge.setText(String.valueOf(results.get(position).getDob().getAge()));
        holder.mPhone.setText(results.get(position).getPhone());
        holder.mNat.setText(results.get(position).getNat());

        Log.d("Count", ""+databaseHelper.getCount(results.get(position).getLogin().getUsername()));
        if (databaseHelper.getCount(results.get(position).getLogin().getUsername()) > 0) {
            Log.d("InteractionStatus", ""+databaseHelper.getInteractionStatus(results.get(position).getLogin().getUsername()));

            if (databaseHelper.getInteractionStatus(results.get(position).getLogin().getUsername()) == 1) {
                if (databaseHelper.getInteractedStatus(results.get(position).getLogin().getUsername()) == 1) {
                    holder.mTxtStatus.setText(context.getString(R.string.accepted));
                } else {
                    holder.mTxtStatus.setText(context.getString(R.string.declined));
                }

                holder.mAccept.setVisibility(View.GONE);
                holder.mDecline.setVisibility(View.GONE);
                holder.mTxtStatus.setVisibility(View.VISIBLE);
            } else {
                holder.mAccept.setVisibility(View.VISIBLE);
                holder.mDecline.setVisibility(View.VISIBLE);
                holder.mTxtStatus.setVisibility(View.GONE);
            }
        }
        holder.mAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int update = databaseHelper.updateInteraction(results.get(position).getLogin().getUsername(),true);
                Log.d("Update", "onClick: "+update);
                Toast.makeText(context, "Profile Accepted!!!",Toast.LENGTH_LONG).show();
                holder.mTxtStatus.setText(context.getString(R.string.accepted));
                holder.mAccept.setVisibility(View.GONE);
                holder.mDecline.setVisibility(View.GONE);
                holder.mTxtStatus.setVisibility(View.VISIBLE);
            }
        });
        holder.mDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int update = databaseHelper.updateInteraction(results.get(position).getLogin().getUsername(),false);
                Log.d("Update", "onClick: "+update);
                Toast.makeText(context, "Profile Declined",Toast.LENGTH_LONG).show();
                holder.mTxtStatus.setText(context.getString(R.string.declined));
                holder.mAccept.setVisibility(View.GONE);
                holder.mDecline.setVisibility(View.GONE);
                holder.mTxtStatus.setVisibility(View.VISIBLE);

            }
        });

        databaseHelper.closeDB();
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView mName,mAddress,mGender,mEmail,mDob,mPhone,mNat,mAge,mTxtStatus;
        private AppCompatImageView mImgProfile;
        private AppCompatButton mAccept, mDecline;

        public ViewHolder(View itemView) {
            super(itemView);
            mImgProfile = itemView.findViewById(R.id.mImgProfile);
            mName = itemView.findViewById(R.id.mTxtName);
            mAddress = itemView.findViewById(R.id.mTxtAddress);
            mGender = itemView.findViewById(R.id.mTxtGender);
            mEmail = itemView.findViewById(R.id.mTxtEmail);
            mDob = itemView.findViewById(R.id.mTxtDob);
            mAge = itemView.findViewById(R.id.mTxtAge);
            mPhone = itemView.findViewById(R.id.mTxtPhone);
            mNat = itemView.findViewById(R.id.mTxtNat);
            mAccept = itemView.findViewById(R.id.mAccept);
            mDecline = itemView.findViewById(R.id.mDecline);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
        }
    }
}
