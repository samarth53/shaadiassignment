package com.shaadi.people.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.shaadi.people.Models.Coordinates;
import com.shaadi.people.Models.Dob;
import com.shaadi.people.Models.Id;
import com.shaadi.people.Models.Location;
import com.shaadi.people.Models.Login;
import com.shaadi.people.Models.Name;
import com.shaadi.people.Models.Picture;
import com.shaadi.people.Models.Registered;
import com.shaadi.people.Models.Result;
import com.shaadi.people.Models.Street;
import com.shaadi.people.Models.Timezone;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "shaadiProfile";
    private static final int DATABASE_VERSION = 3;
    private static final String TABLE_PROFILE = "profile";

    private static final String ID = "id";
    private static final String GENDER = "gender";
    private static final String NAME_TITLE = "title";
    private static final String NAME_FIRST = "first";
    private static final String NAME_LAST = "last";
    private static final String LOCATION_STREET_NAME = "street_name";
    private static final String LOCATION_STREET_NUMBER = "street_number";
    private static final String LOCATION_CITY = "city";
    private static final String LOCATION_STATE = "state";
    private static final String LOCATION_COUNTRY = "country";
    private static final String LOCATION_POSTCODE = "postcode";
    private static final String LOCATION_COORDINATES_LATITUDE = "latitude";
    private static final String LOCATION_COORDINATES_LONGITUDE = "longitude";
    private static final String EMAIL = "email";
    private static final String DOB_DATE = "dob_date";
    private static final String DOB_AGE = "age";
    private static final String PHONE = "phone";
    private static final String CELL = "cell";
    private static final String ID_NAME = "id_name";
    private static final String ID_VALUE = "id_value";
    private static final String PICTURE_LARGE = "picture_large";
    private static final String PICTURE_MEDIUM = "picture_medium";
    private static final String PICTURE_THUMBNAIL = "picture_thumbnail";
    private static final String NATIONALITY = "nat";
    private static final String LOCATION_TIMEZONE_OFFSET = "offset";
    private static final String LOCATION_TIMEZONE_DESCRIPTION = "description";
    private static final String LOGIN_UUID = "uuid";
    private static final String LOGIN_USERNAME = "username";
    private static final String LOGIN_PASSWORD = "password";
    private static final String LOGIN_SALT = "salt";
    private static final String LOGIN_MD5 = "md5";
    private static final String LOGIN_SHA1 = "sha1";
    private static final String LOGIN_SHA256 = "sha256";
    private static final String REGISTERED_DATE = "registered_date";
    private static final String REGISTERED_AGE = "registered_age";
    private static final String USER_INTERACTED = "interaction";
    private static final String USER_INTERACTION_VALUE = "interaction_value";


    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CATEGORY_TABLE = "CREATE TABLE " + TABLE_PROFILE + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + GENDER + " TEXT ,"
                + NAME_TITLE + " TEXT ," + NAME_FIRST + " TEXT ," + NAME_LAST + " TEXT ,"
                + LOCATION_STREET_NAME + " TEXT ," + LOCATION_STREET_NUMBER + " INTEGER ,"
                + LOCATION_CITY + " TEXT ," + LOCATION_STATE + " TEXT ," + LOCATION_COUNTRY + " TEXT ,"
                + LOCATION_POSTCODE + " INTEGER ," + LOCATION_COORDINATES_LATITUDE + " VARCHAR ,"
                + LOCATION_COORDINATES_LONGITUDE + " VARCHAR ," + EMAIL + " VARCHAR ," + DOB_DATE + " TEXT ,"
                + DOB_AGE + " INTEGER ," + PHONE + " VARCHAR ," + CELL + " VARCHAR ," + ID_NAME + " TEXT ,"
                + ID_VALUE + " TEXT ," + PICTURE_LARGE + " VARCHAR ," + PICTURE_MEDIUM + " VARCHAR ,"
                + PICTURE_THUMBNAIL + " VARCHAR ," + NATIONALITY + " TEXT ," + LOCATION_TIMEZONE_OFFSET + " VARCHAR ,"
                + LOCATION_TIMEZONE_DESCRIPTION + " VARCHAR ," + LOGIN_UUID + " VARCHAR ," + LOGIN_USERNAME + " VARCHAR ,"
                + LOGIN_PASSWORD + " VARCHAR ," + LOGIN_SALT + " VARCHAR ," + LOGIN_MD5 + " VARCHAR ," + LOGIN_SHA1 + " VARCHAR ,"
                + LOGIN_SHA256 + " VARCHAR ," + REGISTERED_DATE + " VARCHAR ," + REGISTERED_AGE + " VARCHAR ,"
                + USER_INTERACTED + " INTEGER DEFAULT 0 ," + USER_INTERACTION_VALUE + " INTEGER "
                + ")";
        sqLiteDatabase.execSQL(CREATE_CATEGORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE);
        onCreate(sqLiteDatabase);
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void insertProfile(Result result) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(GENDER, result.getGender());
        values.put(NAME_TITLE, result.getName().getTitle());
        values.put(NAME_FIRST, result.getName().getFirst());
        values.put(NAME_LAST, result.getName().getLast());
        values.put(LOCATION_STREET_NAME, result.getLocation().getStreet().getName());
        values.put(LOCATION_STREET_NUMBER, result.getLocation().getStreet().getNumber());
        values.put(LOCATION_CITY, result.getLocation().getCity());
        values.put(LOCATION_STATE, result.getLocation().getState());
        values.put(LOCATION_COUNTRY, result.getLocation().getCountry());
        values.put(LOCATION_POSTCODE, result.getLocation().getPostcode());
        values.put(LOCATION_COORDINATES_LATITUDE, result.getLocation().getCoordinates().getLatitude());
        values.put(LOCATION_COORDINATES_LONGITUDE, result.getLocation().getCoordinates().getLongitude());
        values.put(EMAIL, result.getEmail());
        values.put(DOB_DATE, result.getDob().getDate());
        values.put(DOB_AGE, result.getDob().getAge());
        values.put(PHONE, result.getPhone());
        values.put(CELL, result.getCell());
        values.put(ID_NAME, result.getId().getName());
        values.put(ID_VALUE, result.getId().getValue());
        values.put(PICTURE_LARGE, result.getPicture().getLarge());
        values.put(PICTURE_MEDIUM, result.getPicture().getMedium());
        values.put(PICTURE_THUMBNAIL, result.getPicture().getThumbnail());
        values.put(NATIONALITY, result.getNat());
        values.put(LOCATION_TIMEZONE_OFFSET, result.getLocation().getTimezone().getOffset());
        values.put(LOCATION_TIMEZONE_DESCRIPTION, result.getLocation().getTimezone().getDescription());
        values.put(LOGIN_UUID, result.getLogin().getUuid());
        values.put(LOGIN_USERNAME, result.getLogin().getUsername());
        values.put(LOGIN_PASSWORD, result.getLogin().getPassword());
        values.put(LOGIN_SALT, result.getLogin().getSalt());
        values.put(LOGIN_MD5, result.getLogin().getMd5());
        values.put(LOGIN_SHA1, result.getLogin().getSha1());
        values.put(LOGIN_SHA256, result.getLogin().getSha256());
        values.put(REGISTERED_DATE, result.getRegistered().getDate());
        values.put(REGISTERED_AGE, result.getRegistered().getAge());


        // insert row
        long insert_id = db.insert(TABLE_PROFILE, null, values);
    }

    public List<Result> getAllProfile(){
        List<Result> results = new ArrayList<Result>();

        String query = "SELECT * FROM "+ TABLE_PROFILE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()){
            do {

                Result result = new Result();
                Name name = new Name();
                Location location = new Location();
                Street street = new Street();
                Coordinates coordinates = new Coordinates();
                Timezone timezone = new Timezone();
                Login login = new Login();
                Dob dob = new Dob();
                Registered registered = new Registered();
                Id id = new Id();
                Picture picture = new Picture();

                result.setGender(cursor.getString(cursor.getColumnIndex(GENDER)));
                name.setTitle(cursor.getString(cursor.getColumnIndex(NAME_TITLE)));
                name.setFirst(cursor.getString(cursor.getColumnIndex(NAME_FIRST)));
                name.setLast(cursor.getString(cursor.getColumnIndex(NAME_LAST)));
                result.setName(name);
                street.setNumber(cursor.getInt(cursor.getColumnIndex(LOCATION_STREET_NUMBER)));
                street.setName(cursor.getString(cursor.getColumnIndex(LOCATION_STREET_NAME)));
                coordinates.setLatitude(cursor.getString(cursor.getColumnIndex(LOCATION_COORDINATES_LATITUDE)));
                coordinates.setLongitude(cursor.getString(cursor.getColumnIndex(LOCATION_COORDINATES_LONGITUDE)));
                timezone.setOffset(cursor.getString(cursor.getColumnIndex(LOCATION_TIMEZONE_OFFSET)));
                timezone.setDescription(cursor.getString(cursor.getColumnIndex(LOCATION_TIMEZONE_DESCRIPTION)));
                location.setStreet(street);
                location.setCoordinates(coordinates);
                location.setTimezone(timezone);
                location.setCity(cursor.getString(cursor.getColumnIndex(LOCATION_CITY)));
                location.setState(cursor.getString(cursor.getColumnIndex(LOCATION_STATE)));
                location.setCountry(cursor.getString(cursor.getColumnIndex(LOCATION_COUNTRY)));
                location.setPostcode(cursor.getString(cursor.getColumnIndex(LOCATION_POSTCODE)));
                result.setLocation(location);
                result.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
                login.setUuid(cursor.getString(cursor.getColumnIndex(LOGIN_UUID)));
                login.setUsername(cursor.getString(cursor.getColumnIndex(LOGIN_USERNAME)));
                login.setPassword(cursor.getString(cursor.getColumnIndex(LOGIN_PASSWORD)));
                login.setSalt(cursor.getString(cursor.getColumnIndex(LOGIN_SALT)));
                login.setMd5(cursor.getString(cursor.getColumnIndex(LOGIN_MD5)));
                login.setSha1(cursor.getString(cursor.getColumnIndex(LOGIN_SHA1)));
                login.setSha256(cursor.getString(cursor.getColumnIndex(LOGIN_SHA256)));
                result.setLogin(login);
                dob.setDate(cursor.getString(cursor.getColumnIndex(DOB_DATE)));
                dob.setAge(cursor.getInt(cursor.getColumnIndex(DOB_AGE)));
                result.setDob(dob);
                registered.setDate(cursor.getString(cursor.getColumnIndex(REGISTERED_DATE)));
                registered.setAge(cursor.getInt(cursor.getColumnIndex(REGISTERED_AGE)));
                result.setRegistered(registered);
                result.setPhone(cursor.getString(cursor.getColumnIndex(PHONE)));
                result.setCell(cursor.getString(cursor.getColumnIndex(CELL)));
                result.setNat(cursor.getString(cursor.getColumnIndex(NATIONALITY)));
                id.setName(cursor.getString(cursor.getColumnIndex(ID_NAME)));
                id.setValue(cursor.getString(cursor.getColumnIndex(ID_VALUE)));
                result.setId(id);
                picture.setLarge(cursor.getString(cursor.getColumnIndex(PICTURE_LARGE)));
                picture.setMedium(cursor.getString(cursor.getColumnIndex(PICTURE_MEDIUM)));
                picture.setThumbnail(cursor.getString(cursor.getColumnIndex(PICTURE_THUMBNAIL)));
                result.setPicture(picture);

                results.add(result);
            }while (cursor.moveToNext());
        }

        return results;
    }

    public Result getProfile(String email){
        String query = "SELECT * FROM "+ TABLE_PROFILE + " WHERE "+ EMAIL + " = "+ email;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null)
            cursor.moveToFirst();

        Result result = new Result();
        Name name = new Name();
        Location location = new Location();
        Street street = new Street();
        Coordinates coordinates = new Coordinates();
        Timezone timezone = new Timezone();
        Login login = new Login();
        Dob dob = new Dob();
        Registered registered = new Registered();
        Id id = new Id();
        Picture picture = new Picture();

        result.setGender(cursor.getString(cursor.getColumnIndex(GENDER)));
        name.setTitle(cursor.getString(cursor.getColumnIndex(NAME_TITLE)));
        name.setFirst(cursor.getString(cursor.getColumnIndex(NAME_FIRST)));
        name.setLast(cursor.getString(cursor.getColumnIndex(NAME_LAST)));
        result.setName(name);
        street.setNumber(cursor.getInt(cursor.getColumnIndex(LOCATION_STREET_NUMBER)));
        street.setName(cursor.getString(cursor.getColumnIndex(LOCATION_STREET_NAME)));
        coordinates.setLatitude(cursor.getString(cursor.getColumnIndex(LOCATION_COORDINATES_LATITUDE)));
        coordinates.setLongitude(cursor.getString(cursor.getColumnIndex(LOCATION_COORDINATES_LONGITUDE)));
        timezone.setOffset(cursor.getString(cursor.getColumnIndex(LOCATION_TIMEZONE_OFFSET)));
        timezone.setDescription(cursor.getString(cursor.getColumnIndex(LOCATION_TIMEZONE_DESCRIPTION)));
        location.setStreet(street);
        location.setCoordinates(coordinates);
        location.setTimezone(timezone);
        location.setCity(cursor.getString(cursor.getColumnIndex(LOCATION_CITY)));
        location.setState(cursor.getString(cursor.getColumnIndex(LOCATION_STATE)));
        location.setCountry(cursor.getString(cursor.getColumnIndex(LOCATION_COUNTRY)));
        location.setPostcode(cursor.getString(cursor.getColumnIndex(LOCATION_POSTCODE)));
        result.setLocation(location);
        result.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        login.setUuid(cursor.getString(cursor.getColumnIndex(LOGIN_UUID)));
        login.setUsername(cursor.getString(cursor.getColumnIndex(LOGIN_USERNAME)));
        login.setPassword(cursor.getString(cursor.getColumnIndex(LOGIN_PASSWORD)));
        login.setSalt(cursor.getString(cursor.getColumnIndex(LOGIN_SALT)));
        login.setMd5(cursor.getString(cursor.getColumnIndex(LOGIN_MD5)));
        login.setSha1(cursor.getString(cursor.getColumnIndex(LOGIN_SHA1)));
        login.setSha256(cursor.getString(cursor.getColumnIndex(LOGIN_SHA256)));
        result.setLogin(login);
        dob.setDate(cursor.getString(cursor.getColumnIndex(DOB_DATE)));
        dob.setAge(cursor.getInt(cursor.getColumnIndex(DOB_AGE)));
        result.setDob(dob);
        registered.setDate(cursor.getString(cursor.getColumnIndex(REGISTERED_DATE)));
        registered.setAge(cursor.getInt(cursor.getColumnIndex(REGISTERED_AGE)));
        result.setRegistered(registered);
        result.setPhone(cursor.getString(cursor.getColumnIndex(PHONE)));
        result.setCell(cursor.getString(cursor.getColumnIndex(CELL)));
        result.setNat(cursor.getString(cursor.getColumnIndex(NATIONALITY)));
        id.setName(cursor.getString(cursor.getColumnIndex(ID_NAME)));
        id.setValue(cursor.getString(cursor.getColumnIndex(ID_VALUE)));
        result.setId(id);
        picture.setLarge(cursor.getString(cursor.getColumnIndex(PICTURE_LARGE)));
        picture.setMedium(cursor.getString(cursor.getColumnIndex(PICTURE_MEDIUM)));
        picture.setThumbnail(cursor.getString(cursor.getColumnIndex(PICTURE_THUMBNAIL)));
        result.setPicture(picture);

        return result;
    }

    public int updateInteraction(String username, boolean interaction){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_INTERACTED, 1);
        if (interaction){
            values.put(USER_INTERACTION_VALUE, 1);
        }else {
            values.put(USER_INTERACTION_VALUE, 0);
        }
        return db.update(TABLE_PROFILE, values, LOGIN_USERNAME + " = ?",
                new String[]{String.valueOf(username)});
    }

    public int getInteractionStatus(String username){
        String query = "SELECT * FROM "+ TABLE_PROFILE + " WHERE "+ LOGIN_USERNAME + " = '"+ username+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null)
            cursor.moveToFirst();
        int status = cursor.getInt(cursor.getColumnIndex(USER_INTERACTED));
        return status;
    }

    public int getInteractedStatus(String username){
        String query = "SELECT * FROM "+ TABLE_PROFILE + " WHERE "+ LOGIN_USERNAME + " = '"+ username+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null)
            cursor.moveToFirst();
        int status = cursor.getInt(cursor.getColumnIndex(USER_INTERACTION_VALUE));
        return status;
    }

    public int getCount(String username){
        String countQuery = "SELECT * FROM " + TABLE_PROFILE + " WHERE "+ LOGIN_USERNAME + " = '"+ username+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public void deleteAll(){
        String query = "DELETE FROM "+ TABLE_PROFILE;
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PROFILE,null,null);
    }

}
