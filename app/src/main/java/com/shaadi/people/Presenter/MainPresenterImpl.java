package com.shaadi.people.Presenter;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.shaadi.people.Helper.DatabaseHelper;
import com.shaadi.people.Models.Profile;
import com.shaadi.people.Models.Result;
import com.shaadi.people.Network.NetworkImpl;
import com.shaadi.people.Network.OnResponseObjectListener;
import com.shaadi.people.R;
import com.shaadi.people.Utils.Utility;
import com.shaadi.people.View.MainView;

import org.json.JSONObject;

public class MainPresenterImpl implements MainPresenter, OnResponseObjectListener {

    private MainView mainView;
    private NetworkImpl network;
    private Context context;
    DatabaseHelper databaseHelper;

    public MainPresenterImpl(Context context,MainView mainView){
        this.context= context;
        this.mainView = mainView;
        network = new NetworkImpl(context);
        databaseHelper = new DatabaseHelper(context);
    }


    @Override
    public void getAllData() {
        network.callJSONObjectAPI(Request.Method.GET,"https://randomuser.me/api/?results=10",null,"GetProfile",this);
    }

    @Override
    public void getDBData() {
        mainView.getDBData(databaseHelper.getAllProfile());
    }

    @Override
    public void onSuccess(JSONObject response) {
        Profile profile = new Gson().fromJson(response.toString(),Profile.class);

        databaseHelper.deleteAll();
        for(Result result : profile.getResults()){
            databaseHelper.insertProfile(result);
        }
        databaseHelper.closeDB();
        mainView.onAPISuccess(profile);
    }

    @Override
    public void onError(VolleyError error) {
        mainView.onAPIError(context.getString(R.string.api_error));
    }
}
