package com.shaadi.people.Network;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface OnResponseObjectListener {

    void onSuccess(JSONObject response);

    void onError(VolleyError error);
}
