package com.shaadi.people.Network;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class NetworkImpl {

    Context context;
    RequestQueue queue;

    public NetworkImpl(Context context){
        this.context = context;
        queue  = Volley.newRequestQueue(context);
    }

    public void callJSONObjectAPI(int requestMethod, String url, JSONObject requestObj, final String alias,final OnResponseObjectListener onResponseObjectListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, url, requestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(alias, "onResponse: "+response);
                onResponseObjectListener.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(alias, "onErrorResponse: "+error.getMessage() );
                onResponseObjectListener.onError(error);
            }
        });
        queue.add(jsonObjectRequest);
    }
}
