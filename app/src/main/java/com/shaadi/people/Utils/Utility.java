package com.shaadi.people.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Utility {

    public static String date(String date){

        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

        String outputPattern = "yyyy-MM-dd HH:mm:ss";

        Date inputDate = null;
        String outputDate = null;


        SimpleDateFormat inputFormatter = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormatter = new SimpleDateFormat(outputPattern, Locale.getDefault());
        //DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(outputPattern, Locale.ENGLISH);
        try {
            inputDate = inputFormatter.parse(date);
            outputDate = outputFormatter.format(inputDate);
        }catch (Exception e){
            e.printStackTrace();
        }

        return outputDate;
    }

    public static boolean checkConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connMgr != null) {
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

            if (activeNetworkInfo != null) { // connected to the internet
                // connected to the mobile provider's data plan
                if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    return true;
                } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
            }
        }
        return false;
    }
}
