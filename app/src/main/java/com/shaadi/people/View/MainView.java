package com.shaadi.people.View;

import com.shaadi.people.Models.Profile;
import com.shaadi.people.Models.Result;

import java.util.List;

public interface MainView {

    void onAPISuccess(Profile profile);
    void onAPIError(String error);
    void getDBData(List<Result> results);
}
