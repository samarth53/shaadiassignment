package com.shaadi.people.Activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.shaadi.people.Adapter.HomeAdapter;
import com.shaadi.people.Models.Profile;
import com.shaadi.people.Models.Result;
import com.shaadi.people.Presenter.MainPresenter;
import com.shaadi.people.Presenter.MainPresenterImpl;
import com.shaadi.people.R;
import com.shaadi.people.Utils.Utility;
import com.shaadi.people.View.MainView;
import com.shaadi.people.databinding.ActivityHomeBinding;

import java.util.List;

public class HomeActivity extends AppCompatActivity implements MainView {

    ActivityHomeBinding activityHomeBinding;
    Context context;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityHomeBinding = DataBindingUtil.setContentView(this,R.layout.activity_home);
        context = HomeActivity.this;
        activityHomeBinding.rcView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mainPresenter = new MainPresenterImpl(context,this);
        activityHomeBinding.progressCircular.setVisibility(View.VISIBLE);
        if (Utility.checkConnection(context)){
            mainPresenter.getAllData();
        }else{
            mainPresenter.getDBData();
        }
    }

    @Override
    public void onAPISuccess(Profile profile) {
        activityHomeBinding.progressCircular.setVisibility(View.GONE);
        HomeAdapter homeAdapter = new HomeAdapter(context,profile.getResults());
        homeAdapter.notifyDataSetChanged();
        activityHomeBinding.rcView.setAdapter(homeAdapter);
    }

    @Override
    public void onAPIError(String error) {
        activityHomeBinding.progressCircular.setVisibility(View.GONE);
        Toast.makeText(context, getString(R.string.error_message),Toast.LENGTH_LONG);
    }

    @Override
    public void getDBData(List<Result> results) {
        activityHomeBinding.progressCircular.setVisibility(View.GONE);
        HomeAdapter homeAdapter = new HomeAdapter(context,results);
        homeAdapter.notifyDataSetChanged();
        activityHomeBinding.rcView.setAdapter(homeAdapter);
    }
}
